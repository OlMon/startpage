#+TITLE: OlMon's Startpage
#+AUTHOR: Marco Pawłowski
#+EMAIL: pawlowski.marco@gmail.com
#+OPTIONS: toc:nil tags:t num:nil
#+EXPORT_FILE_NAME: index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="startpage.css"/>
#+HTML_HEAD: <script src="startpage.js"></script>
* 
** 
:PROPERTIES:
:CUSTOM_ID: header_banner
:END:
*** 
:PROPERTIES:
:CUSTOM_ID: clock
:END:
** 
:PROPERTIES:
:CUSTOM_ID: boxes
:END: 
*** Entertainment
**** [[https://www.primewire.tf/][Prime Wire]]
**** [[https://dosmovies.com/subscriptions/][DosMovies]]
**** [[https://www.WCOforever.Com][Cartoon Cartoons]]
**** [[https://www.twitch.tv/][Twitch TV]]
*** Work
**** [[https://lms.fh-wedel.de/][Moodle]]
**** [[https://www.fh-wedel.de/][Fh Wedel]]
**** [[https://mail.fh-wedel.de/][Fh Mail]]
**** [[https://outlook.office.com/mail/][Outlook]]
**** [[https://campus.verwaltung.fh-wedel.de/][CAS]]
**** [[https://intern.fh-wedel.de/][Intern]]
*** Games
**** [[https://www.fanatical.com/en/][Fanatical]]
**** [[https://www.humblebundle.com/][Humblebundle]]
**** [[https://www.epicgames.com/store/en-US/][Epic Games]]
**** [[https://www.gog.com/][GoG]]
**** [[https://store.steampowered.com/][Steam]]
*** Projects
**** [[https://gitlab.com/][GitLab]]
**** [[https://github.com/][GitHub]]
**** [[https://homedistiller.org/wiki/index.php/Main_Page][Homedistiller]]
*** Server (Data)
**** [[https://nc.olmon.de/][Nextcloud]]
**** [[https://papers.olmon.de/][Paperless]]
**** [[https://img.olmon.de/][Immich]]
**** [[https://papu.olmon.de/][Tandoor]]
**** [[https://bar.olmon.de/][Bar-Assistant]]
*** Server (Fun)
**** [[https://jf.olmon.de/][Jellyfin]]
**** [[https://music.olmon.de/][Navidrome]]
**** [[https://bib.olmon.de/][Komgar]]
**** [[https://rss.olmon.de/][FreshRSS]]
*** Server (Arr* Stack)
**** [[https://movie.olmon.de/][Radarr]]
**** [[https://show.olmon.de/][Sonarr]]
**** [[https://tv.olmon.de/][ErsatzTV]]
**** [[https://prosze.olmon.de/][Jellyseerr]]
**** [[https://torrent.olmon.de/][VueTorrent]]
*** Server (System)
**** [[https://ntfy.olmon.de/][Ntfy]]
**** [[https://traefik.olmon.de/][Traefik]]
**** [[https://dns.olmon.de/][Adguard]]
**** [[https://vpn.olmon.de/][Wireguard]]
**** [[https://sub.olmon.de/][Bazarr]]
**** [[https://prowlarr.olmon.de/][Prowlarr]]
