var boxMap;
var boxes;
var inBox = false;
var curBoxChildrenTags;
var curBoxChildren;
const clockInterval = 100

/**
 * Return a string containing the formatted current date and time.
 */
function getDateTime() {
    const dateTime = new Date();
    let day = dateTime.getDate();
    let month = dateTime.getMonth() + 1;
    let hour = dateTime.getHours();
    let minutes = dateTime.getMinutes();
    let seconds = dateTime.getSeconds();

    if (hour < 0) {
        hour = 24 + hour;
    }

    let date = (day < 10 ? '0' + day : day) + '/' + (month < 10 ? '0' + month : month) + '/' + dateTime.getFullYear();
    let time = (hour < 10 ? '0' + hour : hour) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);

    return date + '\n' + time;
}

window.onload = function() {
    var allA = document.getElementsByTagName("a")
    for (let i = 0; i < allA.length; i ++) {
        allA[i].setAttribute("target", "_top");
        allA[i].setAttribute("rel", "noopener noreferrer");
    }
    
    const clockElement = document.getElementById('clock');
    function setClock() {
        clockElement.innerText = getDateTime();
    }
    setClock();

    setInterval(() => {
        setClock();
    }, clockInterval);

    window.boxes = document.getElementsByClassName("outline-4")

    window.boxMap = {};

    for (let i = 1; i < boxes.length; i++) {
        let ele;
        try {
            ele = window.boxes[i].childNodes[1].childNodes[1].childNodes[0].innerHTML;
        } catch {
            ele = undefined;
        }
        if (ele) {
            window.boxMap[window.boxes[i].childNodes[1].childNodes[1].childNodes[0].innerHTML.toUpperCase()] = window.boxes[i];
        }
        
    }


    function changeBoxesTagDisplay (style) {
        for (let i = 1; i < boxes.length; i++) {
            let ele;
            try {
                ele = window.boxes[i].childNodes[1].childNodes[1];
            } catch {
                ele = undefined;
            }
            if (ele) {
                ele.style.display = style;
            }
        }
    }
    
    window.inBox = false;

    document.addEventListener('keyup', (e) => {
        if (!window.inBox) {
            if(window.boxMap[e.key.toUpperCase()]) {
                window.curBoxChildrenTags = boxMap[e.key.toUpperCase()].getElementsByClassName("tag");
                window.curBoxChildren = {};
                //window.curBoxChildrenTags[0].style.display = "none";
                changeBoxesTagDisplay("none");
                for (let i = 1; i < window.curBoxChildrenTags.length; i++) {
                    window.curBoxChildrenTags[i].style.display = "flex";
                    window.curBoxChildren[window.curBoxChildrenTags[i].childNodes[0].innerHTML.toUpperCase()] =  window.curBoxChildrenTags[i].parentElement.childNodes[1];
                    window.inBox = true;
                }
                
            }
        } else {
            if (e.code == "Escape") {
                changeBoxesTagDisplay("flex");
                for (let i = 1; i < window.curBoxChildrenTags.length; i++) {
                    window.curBoxChildrenTags[i].style.display = "none";
                    window.inBox = false;
                }
            } else if(window.curBoxChildren[e.key.toUpperCase()]) {
                changeBoxesTagDisplay("flex");
                for (let i = 1; i < window.curBoxChildrenTags.length; i++) {
                    window.curBoxChildrenTags[i].style.display = "none";
                }
                window.inBox = false;
                window.curBoxChildren[e.key.toUpperCase()].click();
            }
        }
    }
                              

                             );

};
